#!/bin/bash

if [[ -v DOMAIN1 ]] || [[ -v DOMAIN2 ]]; then
        if [[ -v DOMAIN1 ]]; then
                echo "will be signing for domain: ${DOMAIN1}"
                mail_domain1="${DOMAIN1}"
        fi
        if [[ -v DOMAIN2 ]]; then
                echo "will be signing for domain: ${DOMAIN2}"
                mail_domain2="${DOMAIN2}"
        fi
else
        echo "No domains configured"
        echo "Please set DOMAIN1, DOMAIN2 or both as environment variables"
        exit 1
fi

if [[ ! -f /opendkim/keys/mail.txt ]]; then
        echo "no DKIM keypair found. Generating one"
        opendkim-genkey \
        --directory=/opendkim/keys \
        --selector=mail
fi

echo "starting opendkim daemon"

# -k for KeyFile, -s for Selector, -d for Domains -f for dont-fork-and-exit (foreground), -W for Why mode (log decisions) # -W, why-mode, only works with -l which logs to syslog, currently not to stdout. So it won't log anything
exec /usr/sbin/opendkim -W -f -k /opendkim/keys/mail.private -s mail -d "${mail_domain1},${mail_domain2}" -x /opendkim/opendkim.conf
