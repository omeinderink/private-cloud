#!/usr/bin/env python3

import sys
import os
import subprocess
import atexit
import json
import base64

certpath="/letsencrypt/acme.json"
certfile_content = None
postfix_certpath = "/postfix/certs"
my_hostname = os.environ["MYHOSTNAME"]

def get_certfile_content():
    global certfile_content
    with open(certpath) as file:
        try:
            certfile_content = json.loads(file.read())
        except json.decoder.JSONDecodeError as e:
            sys.exit(f"{e}\nSomething went wrong reading the acme.json file. Exiting...")

def certs_exist():
    return not certfile_content["letsencrypt"]["Certificates"] == None
    
def convert_traefik_certs():
    mail_cert = None
    mail_key = None
    for certificate in certfile_content["letsencrypt"]["Certificates"]:
        if certificate["domain"]["main"] == f"{my_hostname}":
            mail_cert = certificate["certificate"]
            mail_key = certificate["key"]

    if mail_cert == None or mail_key == None:
        sys.exit(f"Certificates couldn't be extracted from {certpath}! Exiting...")
    
    with open(f"{postfix_certpath}/letsencrypt.pem", "w") as file: # todo: add date to filename?
        file.write(base64.b64decode(mail_cert).decode())

    with open(f"{postfix_certpath}/letsencrypt.key", "w") as file:
        file.write(base64.b64decode(mail_key).decode())

def update_postconf():
    subprocess.run(["postconf", "-p", f"smtpd_tls_cert_file={postfix_certpath}/letsencrypt.pem"])
    subprocess.run(["postconf", "-p", f"smtpd_tls_key_file={postfix_certpath}/letsencrypt.key"])

def check_ssl():
    get_certfile_content()
    if certs_exist(): # if certs don't exist do nothing, snakeoil certificates are pre-configured and will be used instead
        convert_traefik_certs()
        update_postconf()

def terminate_postfix(postfix_process):
    postfix_process.terminate()


def main():
    if os.path.isfile(certpath):
        check_ssl()
        postfix_process = subprocess.Popen(["postfix", "start-fg"], stdout=sys.stdout, stderr=subprocess.DEVNULL, text=True)
        atexit.register(terminate_postfix, postfix_process) # ask Postfix to terminate when container is stopped
        postfix_process.wait()
    else:
        sys.exit("No file acme.json found at /letsencrypt/acme.json!\nPlease make sure you mount the file from Traefik to this location. Exiting...")

if __name__ == "__main__":
    main()
