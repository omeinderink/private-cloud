# private-cloud
Configuration for my private cloud setup. No guarantees that it will work for you!

## Setup

1. Clone this repository
2. Run `docker-compose up -d postgres` (to avoid a race condition between postgres and nextcloud you should start the db first)
3. Run `docker-compose up -d`. The rest of the application should start and be accessible after everything has been set up (nextcloud will install for some time, snappymail is available immediately).

### Configure cron

For reasons unkown at this point it's not possible to run a second nextcloud container with the cron.sh entrypoint. Thus, a different way of executing /var/www/html/cron.php with the www-data user has to be configured.

This line in root's crontab on the host should suffice:

*/5 * * * * docker exec --user www-data nextcloud php -f /var/www/html/cron.php

## Known issues
edit Nextcloud's config.php after install and add the line
`overwriteprotocol => https`
this setting is related to the reverse proxy and enables app sync
see: https://help.nextcloud.com/t/cannot-grant-access/64566/13
