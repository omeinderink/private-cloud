DOCKER_COMPOSE_COMMAND=COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose

all: clean
	$(DOCKER_COMPOSE_COMMAND) up -d

build: clean
	$(DOCKER_COMPOSE_COMMAND) up -d --build

mail: clean
	$(DOCKER_COMPOSE_COMMAND) up -d --build traefik rainloop postfix dovecot opendkim

postfix: clean
	$(DOCKER_COMPOSE_COMMAND) up -d --build traefik postfix

.PHONY:	clean
clean:
	$(DOCKER_COMPOSE_COMMAND) down -v
